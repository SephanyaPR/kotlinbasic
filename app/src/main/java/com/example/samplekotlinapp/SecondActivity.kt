package com.example.samplekotlinapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        var intent = intent
        var name = intent.getStringExtra("Name")
        var email = intent.getStringExtra("Email")
        var phone = intent.getStringExtra("Phone")

        resultSecondActivityTextview.text = "Name: "+name+"\nEmail: "+email+"\nPhone: "+phone
    }
}