package com.example.samplekotlinapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        saveMainActivityButton.setOnClickListener {
            var name = nameMainActivityEdittext.text.toString()
            var email = emailMainActivityEdittext.text.toString()
            var phone = phoneMainActivityEdittext.text.toString()
            var intent = Intent(this@MainActivity, SecondActivity::class.java)

            intent.putExtra("Name", name)
            intent.putExtra("Email", email)
            intent.putExtra("Phone", phone)

            if (nameMainActivityEdittext.text.toString().trim().isEmpty() || emailMainActivityEdittext.text.toString().trim().isEmpty() || phoneMainActivityEdittext.text.toString().trim().isEmpty()){
                Toast.makeText(applicationContext, "Fill the empty Field", Toast.LENGTH_SHORT).show()
            }else{


                startActivity(intent)
            }
        }
    }
}